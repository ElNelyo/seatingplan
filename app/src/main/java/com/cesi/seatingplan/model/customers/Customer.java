package com.cesi.seatingplan.model.customers;

import com.cesi.seatingplan.model.CurrentAccount;
import com.cesi.seatingplan.service.customers.OnCreate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import java.util.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.cesi.seatingplan.model.bureau.Bureau;

@Entity
public class Customer implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(min = 2, max = 25)
    private String lastName;

    @NotBlank(message = "First Name is mandatory")
    @Size(min = 3, max = 25)
    private String firstName;

    @NotBlank(message = "Password is mandatory", groups = OnCreate.class)
    private String password;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="bureau_id")
    private Bureau bureau;

    public Bureau getBureau() {
        return bureau;
    }

    public void setBureau(Bureau bureau) {
        this.bureau = bureau;
    }

    private ArrayList<CurrentAccount> currentAccounts = new ArrayList<>();

    public Customer() {
    }

    public Customer(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Customer(String lastName, String firstName, String password) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.setPassword(password);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
