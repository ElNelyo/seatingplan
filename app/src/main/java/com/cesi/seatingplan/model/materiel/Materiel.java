package com.cesi.seatingplan.model.materiel;

import com.cesi.seatingplan.service.materiel.OnCreate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Materiel implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(min = 2, max = 50)
    private String nomTypeMateriel;


    public String getNomTypeMateriel(){
        return this.nomTypeMateriel;
    }

    public Long getId() {
        return id;
    }

    public void setNomTypeMateriel(String nomTypeMateriel) {
        this.nomTypeMateriel = nomTypeMateriel;
    }
}