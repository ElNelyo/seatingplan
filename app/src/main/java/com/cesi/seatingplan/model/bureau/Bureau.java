package com.cesi.seatingplan.model.bureau;


import com.cesi.seatingplan.service.bureau.OnCreate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import com.cesi.seatingplan.model.plan.Plan;
import com.cesi.seatingplan.model.customers.Customer;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import java.util.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@Entity
public class Bureau implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(min = 2, max = 25)
    private String name;

    @NotBlank
    private Long floorsNumber;

    @NotBlank
    private Long quantitePlaces;

    @JsonManagedReference
    @OneToMany(mappedBy="bureau")
    private List<Customer> customers;

    @NotBlank
    @Column(columnDefinition="tinyint(1) default 0")
    private Boolean estOccupe;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="plan_id")
    private Plan plan;

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }


    public Boolean getEstOccupe() { return estOccupe; }
    public Long getQuantitePlaces() { return quantitePlaces; }
    public Long getFloorsNumber(){ return this.floorsNumber;}
    public String getName() { return name; }
    public Long getId() { return id; }
    public Plan getPlan() { return plan; }

    public void setFloorsNumber(Long floorsNumber) { this.floorsNumber = floorsNumber; }
    public void setName(String name) { this.name = name; }
    public void setEstOccupe(Boolean estOccupe) { this.estOccupe = estOccupe; }
    public void setQuantitePlaces(Long quantitePlaces) { this.quantitePlaces = quantitePlaces; }

}