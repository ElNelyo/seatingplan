package com.cesi.seatingplan.model.batiment;

import com.cesi.seatingplan.service.batiment.OnCreate;
import com.cesi.seatingplan.model.etage.Etage;
import com.cesi.seatingplan.model.picture.Picture;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.JoinColumn;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


@Entity
public class Batiment implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(min = 2, max = 50)
    private String nom;

    @JsonManagedReference
    @OneToMany(mappedBy="batiment")
    private List<Etage> etages;


    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="picture_id")
    private Picture picture;

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public List<Etage> getEtages() {
        return etages;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setEtages(List<Etage> etages) {
        this.etages = etages;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}