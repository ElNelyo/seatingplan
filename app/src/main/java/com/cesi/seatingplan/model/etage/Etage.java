package com.cesi.seatingplan.model.etage;

import com.cesi.seatingplan.service.etage.OnCreate;
import com.cesi.seatingplan.model.batiment.Batiment;
import com.cesi.seatingplan.model.plan.Plan;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@Entity
public class Etage implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @JsonManagedReference
    @OneToMany(mappedBy="etage")
    private List<Plan> plans;

    public List<Plan> getPlans() {
        return plans;
    }

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="batiment_id")
    private Batiment batiment;

    public Batiment getBatiment() {
        return batiment;
    }

    public Long getId() {
        return id;
    }
}