package com.cesi.seatingplan.model.plan;

import com.cesi.seatingplan.service.plan.OnCreate;
import com.cesi.seatingplan.model.plan.Plan;
import com.cesi.seatingplan.model.bureau.Bureau;
import com.cesi.seatingplan.model.etage.Etage;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


@Entity
public class Plan implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Size(min = 2, max = 50)
    private String nom;

    @NotBlank
    private Long receptionMaxcapacity;

    @NotBlank
    private Long currentReceptionCapacity;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="etage_id")
    private Etage etage;

    @JsonManagedReference
    @OneToMany(mappedBy="plan")
    private List<Bureau> bureaux;
    public List<Bureau> getBureaux() {
        return bureaux;
    }

    public Long getCurrentReceptionCapacity() {
        return currentReceptionCapacity;
    }

    public Long getReceptionMaxcapacity() {
        return receptionMaxcapacity;
    }

    public Etage getEtage() {
        return etage;
    }

    public void setCurrentReceptionCapacity(Long currentReceptionCapacity) {
        this.currentReceptionCapacity = currentReceptionCapacity;
    }

    public void setReceptionMaxcapacity(Long receptionMaxcapacity) {
        this.receptionMaxcapacity = receptionMaxcapacity;
    }

    public void setBureaux(List<Bureau> bureaux) {
        this.bureaux = bureaux;
    }

    public void setEtage(Etage etage) {
        this.etage = etage;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}