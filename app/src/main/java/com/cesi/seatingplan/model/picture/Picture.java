package com.cesi.seatingplan.model.picture;

import com.cesi.seatingplan.service.picture.OnCreate;
import com.cesi.seatingplan.model.picture.Picture;
import com.cesi.seatingplan.model.batiment.Batiment;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.persistence.OneToMany;


@Entity
public class Picture implements java.io.Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String alt;

    @NotBlank
    private String url;

    @JsonBackReference
    @OneToMany(mappedBy="picture")
    private List<Batiment> batiments;

    public List<Batiment> getBatiments() {
        return batiments;
    }

    public void setBatiments(List<Batiment> batiments) {
        this.batiments = batiments;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getAlt() {
        return alt;
    }

    public Long getId() {
        return id;
    }
}