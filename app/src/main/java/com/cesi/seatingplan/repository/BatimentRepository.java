package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.batiment.Batiment;
import org.springframework.data.repository.CrudRepository;

public interface BatimentRepository extends CrudRepository<Batiment, Long> {

}
