package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.picture.Picture;
import org.springframework.data.repository.CrudRepository;

public interface PictureRepository extends CrudRepository<Picture, Long> {

}
