package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.etage.Etage;
import org.springframework.data.repository.CrudRepository;

public interface EtageRepository extends CrudRepository<Etage, Long> {

}
