package com.cesi.seatingplan.repository;

import com.cesi.seatingplan.model.bureau.Bureau;
import org.springframework.data.repository.CrudRepository;

public interface BureauRepository extends CrudRepository<Bureau, Long> {

}
