package com.cesi.seatingplan.dto.bureau;

public class BureauDTO {
    private Long id;
    private String name;
    private Long floorsNumber;
    private Boolean estOccupe;
    private Long quantitePlaces;

    public Long getId() {
        return id;
    }
    public String getName() { return name; }
    public Long getFloorsNumber() {
        return floorsNumber;
    }
    public Boolean getEstOccupe() { return estOccupe; }
    public Long getQuantitePlaces() { return quantitePlaces; }

    public void setEstOccupe(Boolean estOccupe) { this.estOccupe = estOccupe; }
    public void setQuantitePlaces(Long quantitePlaces) { this.quantitePlaces = quantitePlaces; }
    public void setName(String name) { this.name = name; }
    public void setFloorsNumber(Long floorsNumber) {
        this.floorsNumber = floorsNumber;
    }


}
