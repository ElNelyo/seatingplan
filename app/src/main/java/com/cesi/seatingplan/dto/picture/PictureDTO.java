package com.cesi.seatingplan.dto.picture;

public class PictureDTO {
    private Long id;
    private String url;
    private String alt;

    public Long getId() {
        return id;
    }

    public String getAlt() {
        return alt;
    }

    public String getUrl() {
        return url;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
