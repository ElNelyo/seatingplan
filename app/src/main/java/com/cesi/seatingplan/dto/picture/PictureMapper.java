package com.cesi.seatingplan.dto.picture;
import com.cesi.seatingplan.model.picture.Picture;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PictureMapper implements IMapper<Picture, PictureDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public PictureDTO map(Picture picture, PictureDTO pictureDTO) {
        return this.getModelMapper().map(picture, pictureDTO.getClass());
    }
}
