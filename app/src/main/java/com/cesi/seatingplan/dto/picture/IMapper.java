package com.cesi.seatingplan.dto.picture;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


@Component
public interface IMapper<Model, DTO> {
    public ModelMapper getModelMapper();
    public DTO map(Model model, DTO dto);
}
