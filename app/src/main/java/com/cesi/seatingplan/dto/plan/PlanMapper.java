package com.cesi.seatingplan.dto.plan;
import com.cesi.seatingplan.model.plan.Plan;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class PlanMapper implements IMapper<Plan, PlanDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public PlanDTO map(Plan plan, PlanDTO planDTO) {
        return this.getModelMapper().map(plan, planDTO.getClass());
    }
}
