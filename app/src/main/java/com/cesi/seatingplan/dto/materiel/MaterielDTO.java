package com.cesi.seatingplan.dto.materiel;

public class MaterielDTO {
    private Long id;
    private String nomTypeMateriel;

    public Long getId() {
        return id;
    }


    public String getNomTypeMateriel() {
        return this.nomTypeMateriel;
    }
    public void setNomTypeMateriel(String nomTypeMateriel) {
        this.nomTypeMateriel = nomTypeMateriel;
    }

}
