package com.cesi.seatingplan.dto.materiel;
import com.cesi.seatingplan.model.materiel.Materiel;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MaterielMapper implements IMapper<Materiel, MaterielDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public MaterielDTO map(Materiel materiel, MaterielDTO materielDTO) {
        return this.getModelMapper().map(materiel, materielDTO.getClass());
    }
}
