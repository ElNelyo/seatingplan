package com.cesi.seatingplan.dto.batiment;

public class BatimentDTO {
    private Long id;
    private String lastName;

    public Long getId() {
        return id;
    }


    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
