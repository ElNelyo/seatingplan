package com.cesi.seatingplan.dto.etage;
import com.cesi.seatingplan.model.etage.Etage;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class EtageMapper implements IMapper<Etage, EtageDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public EtageDTO map(Etage etage, EtageDTO etageDTO) {
        return this.getModelMapper().map(etage, etageDTO.getClass());
    }
}
