package com.cesi.seatingplan.dto.batiment;
import com.cesi.seatingplan.model.batiment.Batiment;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class BatimentMapper implements IMapper<Batiment, BatimentDTO> {
    @Override
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

    @Override
    public BatimentDTO map(Batiment batiment, BatimentDTO batimentDTO) {
        return this.getModelMapper().map(batiment, batimentDTO.getClass());
    }
}
