package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.etage.EtageDTO;
import com.cesi.seatingplan.exceptions.EtageNotFoundException;
import com.cesi.seatingplan.exceptions.ResourceNotFoundException;

import com.cesi.seatingplan.model.etage.Etage;
import com.cesi.seatingplan.repository.EtageRepository;
import com.cesi.seatingplan.service.etage.IEtageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0") // This means URL's start with /1.0 (after Application path)
public class EtageController {

    @Autowired
    private IEtageService etageService;

    @Autowired
    private EtageRepository etageRepository;

    @RequestMapping("/etage")
    public @ResponseBody
    Iterable<Etage> all() {
        return etageRepository.findAll();
    }

}