package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.plan.PlanDTO;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.exceptions.ResourceNotFoundException;

import com.cesi.seatingplan.model.plan.Plan;
import com.cesi.seatingplan.repository.PlanRepository;
import com.cesi.seatingplan.service.plan.IPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0") // This means URL's start with /1.0 (after Application path)
public class PlanController {

    @Autowired
    private IPlanService planService;

    @Autowired
    private PlanRepository planRepository;

    @RequestMapping("/plan")
    public @ResponseBody
    Iterable<Plan> all() {
        return planRepository.findAll();
    }

}