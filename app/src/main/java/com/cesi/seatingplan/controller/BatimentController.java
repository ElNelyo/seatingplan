package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.batiment.BatimentDTO;
import com.cesi.seatingplan.exceptions.BatimentNotFoundException;
import com.cesi.seatingplan.exceptions.ResourceNotFoundException;

import com.cesi.seatingplan.model.batiment.Batiment;
import com.cesi.seatingplan.repository.BatimentRepository;
import com.cesi.seatingplan.service.batiment.IBatimentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0") // This means URL's start with /1.0 (after Application path)
public class BatimentController {

    @Autowired
    private IBatimentService batimentService;

    @Autowired
    private BatimentRepository batimentRepository;

    @RequestMapping("/batiment")
    public @ResponseBody
    Iterable<Batiment> all() {
        return batimentRepository.findAll();
    }

}