package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.picture.PictureDTO;
import com.cesi.seatingplan.exceptions.PictureNotFoundException;
import com.cesi.seatingplan.exceptions.ResourceNotFoundException;

import com.cesi.seatingplan.model.picture.Picture;
import com.cesi.seatingplan.repository.PictureRepository;
import com.cesi.seatingplan.service.picture.IPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;


@RestController
@RequestMapping(path="api/1.0") // This means URL's start with /1.0 (after Application path)
public class PictureController {

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private PictureRepository pictureRepository;

    @RequestMapping("/picture")
    public @ResponseBody
    Iterable<Picture> all() {
        return pictureRepository.findAll();
    }

} 