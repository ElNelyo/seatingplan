package com.cesi.seatingplan.controller;

import com.cesi.seatingplan.dto.bureau.BureauDTO;
import com.cesi.seatingplan.exceptions.BureauNotFoundException;
import com.cesi.seatingplan.exceptions.ResourceNotFoundException;

import com.cesi.seatingplan.model.bureau.Bureau;
import com.cesi.seatingplan.repository.BureauRepository;
import com.cesi.seatingplan.service.bureau.IBureauService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="api/1.0") // This means URL's start with /1.0 (after Application path)
public class BureauController {

    @Autowired
    private IBureauService bureauService;

    @Autowired
    private BureauRepository bureauRepository;

    @RequestMapping("/bureau")
    public @ResponseBody
    Iterable<Bureau> all() {
        return bureauRepository.findAll();
    }


    @PostMapping(path="/bureau")
    public @ResponseBody BureauDTO create (@Valid @RequestBody Bureau bur) {
        return bureauService.create(bur);
    }

    // GET a single bureau
    @GetMapping(path="/bureau/{id}")
    public Bureau getBureauById(@PathVariable(value ="id") Long bureauId){
        return bureauRepository.findById(bureauId)
                .orElseThrow(() -> new ResourceNotFoundException("Bureau","id", bureauId)) ;
    }
    @GetMapping(path="/bureau/delete/{id}")
    public String deleteBureau(@PathVariable(value ="id") Long bureauId){
        Bureau bureau = bureauRepository.findById(bureauId)
                .orElseThrow(() -> new ResourceNotFoundException("Bureau","id", bureauId)) ;
        bureauRepository.delete(bureau);
        return "deleted";
    }

    @RequestMapping("/bureau/delete/all")
    public @ResponseBody String deleteAll() {
        Iterable <Bureau> bureaux = bureauRepository.findAll();
        for ( Bureau bureau : bureaux ) {
            bureauRepository.delete(bureau);
        }
        return "all deteled";
    }


}