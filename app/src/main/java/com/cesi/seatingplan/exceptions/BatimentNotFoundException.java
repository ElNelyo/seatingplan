package com.cesi.seatingplan.exceptions;

public class BatimentNotFoundException extends Exception {
    public BatimentNotFoundException(Long id) {
        super("Le bâtiment avec l'id " + id + " n'existe pas");
    }
}
