package com.cesi.seatingplan.exceptions;

public class PlanNotFoundException extends Exception {
    public PlanNotFoundException(Long id) {
        super("Le plan  avec l'id " + id + " n'existe pas");
    }
}
