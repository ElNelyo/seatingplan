package com.cesi.seatingplan.exceptions;

public class EtageNotFoundException extends Exception {
    public EtageNotFoundException(Long id) {
        super("L'étage  avec l'id " + id + " n'existe pas");
    }
}
