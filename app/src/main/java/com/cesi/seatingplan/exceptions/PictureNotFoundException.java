package com.cesi.seatingplan.exceptions;

public class PictureNotFoundException extends Exception {
    public PictureNotFoundException(Long id) {
        super("L'image  avec l'id " + id + " n'existe pas");
    }
}
