package com.cesi.seatingplan.service.etage;

import com.cesi.seatingplan.dto.etage.EtageDTO;
import com.cesi.seatingplan.dto.etage.EtageMapper;
import com.cesi.seatingplan.exceptions.EtageNotFoundException;
import com.cesi.seatingplan.model.etage.Etage;
import com.cesi.seatingplan.repository.EtageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class EtageService implements IEtageService {
    @Autowired
    private EtageRepository etageRepository;

    //@Autowired
    //private Password password;

    private EtageMapper etageMapper = new EtageMapper();

    @Override
    public EtageDTO getById(Long id) throws EtageNotFoundException {
        Optional<Etage> found = etageRepository.findById(id);
        if (found.isPresent()) {
            return etageMapper.map(found.get(), new EtageDTO());
        } else {
            throw new EtageNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public EtageDTO create(@Valid Etage etage) {
        // TODO MAKE THIS BREAK FFS ......
        //customer.setPassword(password.hashPassword(customer.getPassword()));
        return etageMapper.map(etageRepository.save(etage), new EtageDTO());
    }
}
