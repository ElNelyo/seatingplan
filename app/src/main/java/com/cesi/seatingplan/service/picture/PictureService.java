package com.cesi.seatingplan.service.picture;

import com.cesi.seatingplan.dto.picture.PictureDTO;
import com.cesi.seatingplan.dto.picture.PictureMapper;
import com.cesi.seatingplan.exceptions.PictureNotFoundException;
import com.cesi.seatingplan.model.picture.Picture;
import com.cesi.seatingplan.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class PictureService implements IPictureService {
    @Autowired
    private PictureRepository pictureRepository;

    //@Autowired
    //private Password password;

    private PictureMapper pictureMapper = new PictureMapper();

    @Override
    public PictureDTO getById(Long id) throws PictureNotFoundException {
        Optional<Picture> found = pictureRepository.findById(id);
        if (found.isPresent()) {
            return pictureMapper.map(found.get(), new PictureDTO());
        } else {
            throw new PictureNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public PictureDTO create(@Valid Picture picture) {
        // TODO MAKE THIS BREAK FFS ......
        //customer.setPassword(password.hashPassword(customer.getPassword()));
        return pictureMapper.map(pictureRepository.save(picture), new PictureDTO());
    }
}
