package com.cesi.seatingplan.service.plan;

import com.cesi.seatingplan.dto.plan.PlanDTO;
import com.cesi.seatingplan.dto.plan.PlanMapper;
import com.cesi.seatingplan.exceptions.PlanNotFoundException;
import com.cesi.seatingplan.model.plan.Plan;
import com.cesi.seatingplan.repository.PlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class PlanService implements IPlanService {
    @Autowired
    private PlanRepository planRepository;


    private PlanMapper planMapper = new PlanMapper();

    @Override
    public PlanDTO getById(Long id) throws PlanNotFoundException {
        Optional<Plan> found = planRepository.findById(id);
        if (found.isPresent()) {
            return planMapper.map(found.get(), new PlanDTO());
        } else {
            throw new PlanNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public PlanDTO create(@Valid Plan plan) {
        // TODO MAKE THIS BREAK FFS ......
        //customer.setPassword(password.hashPassword(customer.getPassword()));
        return planMapper.map(planRepository.save(plan), new PlanDTO());
    }
}
