package com.cesi.seatingplan.service.etage;

import com.cesi.seatingplan.dto.etage.EtageDTO;
import com.cesi.seatingplan.exceptions.EtageNotFoundException;
import com.cesi.seatingplan.model.etage.Etage;
import javax.validation.Valid;


public interface IEtageService {
    public EtageDTO getById(Long id) throws EtageNotFoundException;
    public EtageDTO create(@Valid Etage etage);
}
