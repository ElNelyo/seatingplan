package com.cesi.seatingplan.service.bureau;

import com.cesi.seatingplan.dto.bureau.BureauDTO;
import com.cesi.seatingplan.dto.bureau.BureauMapper;
import com.cesi.seatingplan.exceptions.BureauNotFoundException;
import com.cesi.seatingplan.model.bureau.Bureau;
import com.cesi.seatingplan.repository.BureauRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;


import javax.validation.Valid;
import java.util.Optional;

@Service
@Validated
public class BureauService implements IBureauService {
    @Autowired
    private BureauRepository bureauRepository;

    //@Autowired
    //private Password password;

    private BureauMapper bureauMapper = new BureauMapper();

    @Override
    public BureauDTO getById(Long id) throws BureauNotFoundException {
        Optional<Bureau> found = bureauRepository.findById(id);
        if (found.isPresent()) {
            return bureauMapper.map(found.get(), new BureauDTO());
        } else {
            throw new BureauNotFoundException(id);
        }
    }

    @Validated(OnCreate.class)
    @Override
    public BureauDTO create(@Valid Bureau bureau) {
        // TODO MAKE THIS BREAK FFS ......
        //customer.setPassword(password.hashPassword(customer.getPassword()));
        return bureauMapper.map(bureauRepository.save(bureau), new BureauDTO());
    }
}
