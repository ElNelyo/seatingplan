package com.cesi.seatingplan.service.picture;

import com.cesi.seatingplan.dto.picture.PictureDTO;
import com.cesi.seatingplan.exceptions.PictureNotFoundException;
import com.cesi.seatingplan.model.picture.Picture;
import javax.validation.Valid;


public interface IPictureService {
    public PictureDTO getById(Long id) throws PictureNotFoundException;
    public PictureDTO create(@Valid Picture picture);
}
