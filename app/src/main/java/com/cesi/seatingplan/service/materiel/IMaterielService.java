package com.cesi.seatingplan.service.materiel;

import com.cesi.seatingplan.dto.materiel.MaterielDTO;
import com.cesi.seatingplan.exceptions.MaterielNotFoundException;
import com.cesi.seatingplan.model.materiel.Materiel;
import javax.validation.Valid;


public interface IMaterielService {
    public MaterielDTO getById(Long id) throws MaterielNotFoundException;
    public MaterielDTO create(@Valid Materiel materiel);
}
